<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArchitectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('architect', function (Blueprint $table) {
            $table->increments('id');
            $table->string('architectlname');
            $table->string('architectfname');
            $table->string('architectaddress');
            $table->string('email')->unique();
            $table->string('username');
            $table->string('password', 60);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
            Schema::drop('architect');
    }
}
