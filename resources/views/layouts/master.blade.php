<!DOCTYPE html>
<html>
<head>
	<title>United Comms a Corporate Category Flat Bootstarp responsive Website Template| Home :: w3layouts</title>
	
	{!! HTML::style('css/bootstrap.min.css') !!}
	{!! HTML::style('ss/dataTables.bootstrap.css') !!}	
	{!! HTML::style('css/bootstrap.css') !!}
	{!! HTML::style('css/style.css') !!}
	{!! HTML::script('css/lightbox.css') !!}
	{!!HTML::style('css/bootstrap-datetimepicker.min.css')!!}
   

	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="United Comms Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!--webfont-->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
</head>
<body>
	<!-- header-section-starts -->
	<div class="header">
		<div class="container">
			<div class="logo">
				<a href="index.html"><img src="images/logo.png" class="img-responsive" alt="" /></a>
			</div>
			<div class="header-right">
			<br><br>
				<span class="menu"></span>
				<div class="top-menu">
					<ul>                                              
						<li><a href="/index">Home</a></li>
						<li><a href="/About">About Us</a></li>
						<li><a href="/apartmentdetails">apartment</a></li>
						<li><a href="/register">Register Tenants</li>
						<li><a href="/occupant">List of Occupants</li>
						<li><a href='Logout'>logout</a></li>
						
						<!-- <ali><a href="/monthlybill">monthly bill</a></li>
						<!-- <li><a href="/monthlymaintenance">monthly maintenance</a></li>
						<li><a href="/individualapartmentdetails">individual apartment details</a></li>
						<li><a href="/desc">Description</a></li>

					 --></ul>


				</div>
				<!-- script for menu -->
				<script>
					$( "span.menu" ).click(function() {
						$( ".top-menu" ).slideToggle( "slow", function() {
				    // Animation complete.
				});
					});
				</script>
				<!-- script for menu -->

			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<!-- header-section-ends -->
  @yield('contents')
	<div class="footer text-center">
		<div class="social-icons">
			<a href="#"><i class="facebook"></i></a>
			<a href="#"><i class="twitter"></i></a>
			<a href="#"><i class="googlepluse"></i></a>
			<a href="#"><i class="youtube"></i></a>
			<a href="#"><i class="linkedin"></i></a>
		</div>
		<div class="copyright">
			<p>Copyright &copy; 2015 All rights reserved | Template by  <a href="http://w3layouts.com">  W3layouts</a></p>
		</div>
	</div>

	{!! HTML::script('js/lightbox.js') !!}
	{!! HTML::script('js/jQuery-2.1.4.min.js') !!}
	{!! HTML::script('js/bootstrap.min.js') !!}
	{!! HTML::script('js/jquery.dataTables.min.js') !!}
	{!! HTML::script('js/dataTables.bootstrap.min.js') !!}
	{!! HTML::script('js/jquery.min.js') !!}

	<script type="text/javascript"
     src="css/jquery.min.js">
    </script> 

    <script type="text/javascript"
     src="/assets/js/bootstrap-datetimepicker.min.js">
    </script>
    <script type="text/javascript"
     src="/assets/js/bootstrap-datetimepicker.pt-BR.js">
    </script>
    <script type="text/javascript">
      $('#datetimepicker1').datetimepicker({
        format: 'dd/MM/yyyy',
        language: 'fr'
      });
    </script>
      <script type="text/javascript">
      $('#datetimepicker2').datetimepicker({
        format: 'dd/MM/yyyy',
        language: 'fr'
      });
    </script>
    <script type="text/javascript">
      $('#datetimepicker').datetimepicker({
        format: 'dd/MM/yyyy',
        language: 'fr'
      });
    </script>

    <script type="text/javascript">
      $(function () {
        $("#example1").dataTable();
        $('#example2').dataTable({
          "bPaginate": true,
          "bLengthChange": false,
          "bFilter": false,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false
        });
      });
    </script>

</body>
</html>