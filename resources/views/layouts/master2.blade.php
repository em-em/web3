<!Doctype html>
<html>
	<head>
		<title>@yield('title')</title>
		
		{!! HTML::style('assets/css/bootstrap-theme.css') !!}
		{!! HTML::style('assets/css/bootstrap-theme.css.map') !!}
		{!! HTML::style('assets/css/bootstrap.min.css') !!}
		{!! HTML::style('assets/css/bootstrap.css') !!}
		{!! HTML::style('assets/css/bootstrap.css.map') !!}
		{!! HTML::style('assets/css/bootstrap.min.css') !!}
		{!! HTML::style('assets/css/bootstrap.js') !!}
		{!! HTML::style('assets/css/bootstrap.min.js') !!}
		
	</head>
<body>
	@yield('contents')
</body>
</html>