
@extends('layouts.master')

@section('contents')

	<div class="container">
		<div class="row col-lg-12" style="margin-top:40px;">

				<div class="col-lg-7 col-lg-offset-3">
					<div class="panel panel-primary">
						<div class="panel-body"><br><br>
				{!! Form::open(['url' => '/create/account']) !!}
					{!! Form::label('apartmentid','Room Number') !!}
					{!! Form::text('apartmentid', $value = null, array('placeholder' => 'Room Number', 'class'=> 'form-control','autofocus' => 'autofocus' )) !!}
					@if ($errors->has('apartmentid'))<p style="color:red;">Required fields/numbers only</p>@endif
		   			{!! Form::label('firstname','First Name') !!}
					{!! Form::text('firstname', $value = null, array('placeholder' => 'Firstname', 'class'=> 'form-control','autofocus' => 'autofocus' )) !!}
					@if ($errors->has('firstname'))<p style="color:red;">Required field/numbers not allowed</p>@endif
					{!! Form::label('lastname','Last Name') !!}
					{!! Form::text('lastname', $value = null, array('placeholder' => 'Lastname', 'class'=> 'form-control','autofocus' => 'autofocus' )) !!}
					@if ($errors->has('lastname'))<p style="color:red;">Required field/numbers not allowed</p>@endif
					{!! Form::label('Address','Address') !!}
					{!! Form::text('address', $value = null, array('placeholder' => 'Address', 'class'=> 'form-control','autofocus' => 'autofocus' )) !!}
					@if ($errors->has('address'))<p style="color:red;">Required field</p>@endif
					{!! Form::label('secretkey','SecretKey') !!}
					{!! Form::password('password', array('class' => 'form-control','placeholder'=>'Secretkey')) !!}
					@if ($errors->has('password'))<p style="color:red;">Required field</p>@endif
					{!! Form::label('lease_start','Lease_Start') !!}
						<div id="datetimepicker1" class="input-append date">
					{!! Form::text('lease_start', $value = null, array('placeholder' => 'Lease_Start', 'class'=> 'form-control','autofocus' => 'autofocus' )) !!}
					
					      <span class="add-on">
					        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
					      </span>
					    </div>
					    @if ($errors->has('lease_start'))<p style="color:red;">Required field</p>@endif
					{!! Form::label('lease_end','Lease_End') !!}
						<div id="datetimepicker2" class="input-append date">
					{!! Form::text('lease_end', $value = null, array('placeholder' => 'Lease_End', 'class'=> 'form-control','autofocus' => 'autofocus' )) !!}
					      <span class="add-on">
					        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
					      </span>
					    </div>
					    @if ($errors->has('lease_end'))<p style="color:red;">Required field</p>@endif
					{!! Form::label('Contact Number','Contact Number') !!}
					{!! Form::text('contactnumber', $value = null, array('placeholder' => 'Contactnumber', 'class'=> 'form-control','autofocus' => 'autofocus' )) !!}
					@if ($errors->has('contactnumber'))<p style="color:red;">Required field/numbers only</p>@endif
					</br>{!! Form::submit('submit') !!}
				{!! Form::close() !!}
			
						</div>
					</div>
			</div>
		</div>
	</div>
	
@stop
