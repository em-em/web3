{!! HTML::style('assets/css/index.css') !!}
@extends('layouts.master2')

@section('title')
	Index Page
@stop

@section('contents')
<div id="black">
</div>

<div id="container">

		<div id="loginfo">
		{!! Form::open(['url' => '/index/login']) !!}
		 {!! csrf_field() !!}
		<table>
			<tr><td>{!! Form::label('username','Apartment No.') !!}</td>
		
				<td>{!! Form::label('password','SecretKey') !!}</td>
			</tr>
		    <tr>
		    	<td>{!! Form::text('apartmentid', $value = null, array('placeholder' => 'Apartment Number', 'class'=> 'form-control', 'required' => 'required', 'autofocus' => 'autofocus' )) !!}</td>
			    <td>{!! Form::password('password', array('class' => 'form-control')) !!}</td>
			    <td style="width:500px">{!! Form::submit('Log In',array('class'=>"btn btn-primary",'name'=>'login')) !!}</td>
			</tr>
			
		
		</table>
	</div>
	<div id="qoutes">
		<i>Our house is built with</i></br>
			<img src="assets/img/beams.png"></br>
		<i>Our home is built with</i></br>
			<img src="assets/img/love.png"></br>

	</div>

</div>

<div id="darkdark">


</div>

@stop
