@extends('layouts.master')

@section('contents')
	<!-- header-section-ends -->
	<div class="content">
		<div class="container">
			<!----> 
		<div class="products">
			<h2 class="heading text-center">Our Products</h2>
			<div class="products-bottom">
					<div class="col-md-6 products-grid">

						<img src="images/product-1.jpg" alt=""/>
						<div class="products-info">
							<a href="single.html">Phasellus scipiw scipitilifen. </a>
							<p>Kuspendisse laoreet augue iderti wer interdum merti oremolo lectusto odio, sedorolu fringilla estero libero orciwe.</p>
						</div>
					</div>
					<div class="col-md-6 products-grid">
						<img class="example-image" src="images/product-2.jpg" alt=""/>
						<div class="products-info">
							<a href="single.html">Phasellus scipiw scipitilifen. </a>
							<p>Kuspendisse laoreet augue iderti wer interdum merti oremolo lectusto odio, sedorolu fringilla estero libero orciwe.</p>
						</div>
					</div>
					<div class="col-md-6 products-grid">
						<img class="example-image" src="images/product-3.jpg" alt=""/>
						<div class="products-info">
							<a href="single.html">Phasellus scipiw scipitilifen. </a>
							<p>Kuspendisse laoreet augue iderti wer interdum merti oremolo lectusto odio, sedorolu fringilla estero libero orciwe.</p>
						</div>
					</div>
					<div class="col-md-6 products-grid">
						<img class="example-image" src="images/product-4.jpg" alt=""/>
						<div class="products-info">
							<a href="single.html">Phasellus scipiw scipitilifen. </a>
							<p>Kuspendisse laoreet augue iderti wer interdum merti oremolo lectusto odio, sedorolu fringilla estero libero orciwe.</p>
						</div>
					</div>
					<div class="col-md-6 products-grid">
						<img class="example-image" src="images/product-5.jpg" alt=""/>
						<div class="products-info">
							<a href="single.html">Phasellus scipiw scipitilifen. </a>
							<p>Kuspendisse laoreet augue iderti wer interdum merti oremolo lectusto odio, sedorolu fringilla estero libero orciwe.</p>
						</div>
					</div>
					<div class="col-md-6 products-grid">
						<img class="example-image" src="images/product-6.jpg" alt=""/>
						<div class="products-info">
							<a href="single.html">Phasellus scipiw scipitilifen. </a>
							<p>Kuspendisse laoreet augue iderti wer interdum merti oremolo lectusto odio, sedorolu fringilla estero libero orciwe.</p>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
	 </div>

		</div>
		</div>
	
	</div>
	@stop