@extends('layouts.master')

@section('contents')
<!-- header-section-ends -->
<div class="content">
	<div class="container">
		<!----> 
		<div class="products">
			<h2 class="heading text-center"></h2>
			<div class="products-bottom">
				<div class="col-md-6 products-grid">
					<img src="images/product-1.jpg" alt=""/>
					<div class="products-info"><br>
						<h2><b>Bachochay Apartment</b></h2>
						<p>Price: <b>10000.00</b></p>
					 <button  type="button" class="btn btn-primary btn-flat">Read More</button>
					</div>
				</div>
				<div class="col-md-6 products-grid">
					<img src="images/product-2.jpg" alt=""/>
					<div class="products-info"><br>
						<h2><b>Kiray Apartment</b></h2>
						<p>Price: <b>10000.00</b></p>
					 <button  type="button" class="btn btn-primary btn-flat">Read More</button>
					</div>
				</div>
				<div class="col-md-6 products-grid">
					<img src="images/product-3.jpg" alt=""/>
					<div class="products-info"><br>
						<h2><b>Boenghaha Apartment</b></h2>
						<p>Price: <b>10000.00</b></p>
					 <button  type="button" class="btn btn-primary btn-flat">Read More</button>
					</div>
				</div>
				<div class="col-md-6 products-grid">
					<img src="images/product-4.jpg" alt=""/>
					<div class="products-info"><br>
						<h2><b>Bongga Apartment</b></h2>
						<p>Price: <b>10000.00</b></p>
					 <button  type="button" class="btn btn-primary btn-flat">Read More</button>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<!----> 

	</div>
</div>

</div>
@stop