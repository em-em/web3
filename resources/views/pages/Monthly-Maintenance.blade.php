@extends('layouts.master')

@section('contents')
	<!-- header-section-ends -->
	<div class="container">
		<div class="row col-lg-12" style="margin-top:40px;">

			<form class="form-horizontal">
				<div class="col-lg-7 col-lg-offset-3" id="divLogin">
					<div class="panel panel-primary">
					<div class="panel-body"><br><br><br>
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-2 control-label">Email</label>
								<div class="col-sm-10">
									<input type="email" class="form-control" id="inputEmail3" placeholder="Email">
								</div>
							</div>
							<br/>
							<div class="form-group">
								<label for="inputPassword3" class="col-sm-2 control-label">Password</label>
								<div class="col-sm-10">
									<input type="password" class="form-control" id="inputPassword3" placeholder="Password">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<button type="submit" class="btn btn-default">Sign in</button>
								</div>
							</div>

							<div class="form-group">
							</div>
							<div  id="reg" style="text-align:center">
								<a href="register.html">Not yet a member? Register Here.</a>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
@stop