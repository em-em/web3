@extends('layouts.master')

@section('contents')
	<div class="content text-center">
		<div class="who_we_are">
			<div class="container">
					<div class="grid_12">
						<header>
							<h3>Rules and Regulations</h3>
						</header>
						<h5>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent vestibulum molestie lacus. Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur</h5>
						<p>Donec sit amet eros. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Mauris fermentum dictum magna. Sed laoreet aliquam leo. Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Integer rutrum ante eu lacus. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent vestibulum molestie lacus. Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi.</p>
						<p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla dui. Fusce feugiat malesuada odio. Morbi nunc odio, gravida at, cursus nec, luctus a, lorem. Maecenas tristique orci ac sem. Duis ultricies pharetra magna. </p>
					</div>
					<div class="grid_5"><img src="images/index-1_img-1.jpg" alt=""></div>
					<div class="grid_6"><img src="images/index-1_img-2.jpg" alt=""></div>
					<div class="clearfix"></div>
			</div>
		</div>
		<div class="why_choose_us">
			<div class="container">
				<div class="row">
						<header>
							<h3>Why you shoud choose Us</h3>
						</header>
					</div>
					<div class="col-md-4 grid_4">
						<div>
							<span>01</span>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent vestibulum molestie lacus. Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur</p>
						</div>
					</div>
					<div class="col-md-4 grid_4">
						<div>
							<span>02</span>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent vestibulum molestie lacus. Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur</p>
		
						</div>
					</div>
					<div class="col-md-4 grid_4">
						<div>
							<span>03</span>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent vestibulum molestie lacus. Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur</p>
			
						</div>
					</div>
			</div>
		</div>
		<div class="latest-news">
			<div class="container">
				<div class="row">
					<header>
						<h3>Our latest news</h3>
					</header>
				</div>
					<div class="col-md-4 grid_7">
						<div class="element">
							<img src="images/index-1_img-3.jpg" alt="">
							<h4>Balay</h4>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent vestibulum molestie lacus. Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur</p>
						</div>
					</div>
					<div class="col-md-4 grid_7">
						<div class="element">
							<img src="images/index-1_img-4.jpg" alt="">
							<h4>balay</h4>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent vestibulum molestie lacus. Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur</p>
						</div>
					</div>
					<div class="col-md-4 grid_7">
						<div class="element">
							<img src="images/index-1_img-5.jpg" alt="">
							<h4>balay</h4>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent vestibulum molestie lacus. Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur</p>
						</div>
					</div>
			</div>
		</div>
		</div>
	</div>
@stop