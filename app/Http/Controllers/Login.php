<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Http\Requests;
use Input;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Hash;
use DB;
class Login extends Controller
{
    
    public function postRegister(LoginRequest $request)
    {  
        $data = new User;
        $data->apartment_id = $request->get('apartmentid');
        $data->firstname = $request->get('firstname');
        $data->lastname = $request->get('lastname');
        $data->address = $request->get('address');
        $data->password = Hash::make($request->get('password'));
        $data->lease_start = $request->get('lease_start');
        $data->lease_end = $request->get('lease_end');
        $data->contactnumber = $request->get('contactnumber');  
       
       
        
        $userexist = DB::table('occupant')->where('apartment_id',$request->get('apartmentid'))->first();

        
        if ($userexist)
        {
            echo "<script>alert('Username exist think another username')</script>";
            return view('pages.Register');
        }
        else
        {
            echo "<script>alert('Successfully Registered')</script>";
             $data->save();
             return view('pages.Register');
             
        }
      
    }

    public function authenticate()
    {       $username = Input::get('apartmentid');
            $password = Input::get('password');
           

            $userexisted = DB::table('occupant')->where('apartment_id', $username)->first();
            if($userexisted){
                if (Auth::attempt(['apartment_id' => $username, 'password' => $password]))
                {   
                   return view('pages.index');
                    
                }
                else{
                    echo "wrong password";
                    

                }

            }    
                // else{
                //     echo "account does not exist";
                // }

    }
   public function index()
    {   
       $data= DB::table('occupant')->get();
        echo "<pre>";
       print_r($data);
       echo "</pre>";

   
       return view('pages.occupants',['data'=>$data]);
        
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
