<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/', function () {
    return view('welcome');
});

Route::get('/index',function(){
	return view('pages.index');
});
Route::post('/index/login','Login@authenticate');

Route::get('/register',function(){
	return view('pages.Register');
});
Route::post('/create/account','Login@postRegister');
Route::get('/occupant','Login@index');

// Route::get('/About',function(){
// 	return view('pages.About');
// });
Route::get('/home',function(){
	return view('pages.home');
});

Route::get('/Login',function(){
	//Auth::logout();
	return view('pages.Login');
});
// Route::get('/index', 'Index@create');
// Route::get('/desc', 'Desc@create');
// Route::get('/login', 'Login@create');
// Route::get('/monthlybill', 'MonthlyBill@create');
// Route::get('/about', 'AboutUs@create');
// Route::get('/apartmentdetails', 'ApartmentDetails@create');
// // Route::get('/register', 'Register@create');
// Route::get('/monthlymaintenance', 'MonthlyMaintenance@create');
// Route::get('/individualapartmentdetails', 'IndividualApartmentDetails@create');
// Route::get('/products', 'Products@create');

// Route::get('/test',function(){
// 	return view('pages.test');
// });